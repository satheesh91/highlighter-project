let dashboard_url = `https://theaisquared.com/app/`;

chrome.tabs.onUpdated.addListener(function(tabId, changeInfo, tab) {
  if (changeInfo.status == "complete") {
    chrome.storage.sync.remove("session_id");
    chrome.storage.sync.remove("folder");
    chrome.storage.sync.remove("folder_id");

    chrome.cookies.getAll({
      url: "https://theaisquared.com/app/"
    }, function(cookies){
      if(cookies.length){
        for(let cookie of cookies){
          if(cookie.name == 'user_id')
            chrome.storage.sync.set({ session_id: cookie.value });
          if(cookie.name == 'folder' || cookie.name == 'folder_id')
            chrome.storage.sync.set({ [cookie.name]: cookie.value });
        }
      }
    });
    // chrome.cookies.get(
    //   {
    //     url: "https://theaisquared.com/app/",
    //     name: "user_id"
    //   },
    //   function(cookie) {
    //     if (cookie != null) {
    //       chrome.storage.sync.set({ session_id: cookie.value });
    //       chrome.cookies.get(
    //         {
    //           url: "https://theaisquared.com/app/",
    //           name: "folder"
    //         },
    //         function(cookie) {
    //           if (cookie != null) {
    //             chrome.storage.sync.set({ folder: cookie.value });
    //           } else {
    //             chrome.storage.sync.remove("folder");
    //           }
    //         }
    //       );
    //     } else {
    //       chrome.storage.sync.remove("session_id");
    //     }
    //   }
    // );
  }
});

chrome.browserAction.onClicked.addListener(function(tab) {
  chrome.storage.sync.remove("session_id");
  chrome.cookies.get(
    {
      url: "https://theaisquared.com/app/",
      name: "user_id"
    },
    function(cookie) {
      if (cookie != null) {
        chrome.storage.sync.set({ session_id: cookie.value });
        chrome.tabs.executeScript(tab.id, { code: `toggleSidePane();` });
      } else {
        chrome.tabs.create({ url: dashboard_url });
      }
    }
  );
});

chrome.webRequest.onHeadersReceived.addListener(
  function(details) {
    if (details.url.indexOf(".pdf") > 0) {
      var viewerUrl =
        chrome.extension.getURL("web/viewer.html") +
        "?file=" +
        decodeURIComponent(details.url);
      return { redirectUrl: viewerUrl };
    }
  },
  {
    urls: ["<all_urls>"],
    types: ["main_frame", "sub_frame"]
  },
  ["responseHeaders", "blocking"]
);
