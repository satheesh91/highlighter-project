// PDFJS.workerSrc = "../build/pdf.worker.js";
let user_id = null;
let folder = null;
let folder_id = 0;
chrome.storage.sync.get(["session_id", "folder", "folder_id"], function (
  result
) {
  if (
    result.session_id != undefined &&
    result.session_id != null &&
    parseInt(result.session_id) > 0
  ) {
    user_id = result.session_id;
    folder = result.folder;
    folder_id = result.folder_id;
    const comment_input_wrap = `
        <div class="col-md-12 input-wrap shadow-sm p-0">
            <div class="form-group p-0 mb-1 mt-2">
                <textarea id="comment" class="form-control" placeholder="Enter your comments" name="comment"></textarea>
            </div>
            <div class="form-group mb-0 pr-0 text-right">
                <button class="btn btn-primary btn-sm btn-submit-comment">Submit</button>
                <button class="btn btn-secondary btn-sm btn-cancel-comment">Cancel</button>
            </div>
        </div>`;

    const loadFolderTree = async () => {
      let result = await fetch(
        `https://theaisquared.com/app/api/folders/list/user/${user_id}`
      );
      let { folders } = await result.json();

      var tree = folders.map((item, index) => {
        return `<li class="list-group-item node-tree ${
          folder_id == item.id ? "active" : ""
        }" data-nodeid="${index}" data-folder_id="${item.id}">
          <span class="icon icon-folder-alt text-white glyphicon"></span>
          <span class="icon icon-folder-alt text-white node-icon fa fa-folder fa-lg mr-2"></span>
          ${item.name}
        </li>`;
      });

      $("#tree").html(`<ul class="list-group">${tree}</ul>`);
    };

    const init = async () => {
      $(".folder_name").html(
        decodeURIComponent((folder + "").replace(/\+/g, "%20"))
      );

      await loadFolderTree();

      // var tree = folders.map((item, index) => {
      //   return `<li class="list-group-item node-tree ${
      //     folder_id == item.id ? "active" : ""
      //   }" data-nodeid="${index}" data-folder_id="${item.id}">
      //     <span class="icon icon-folder-alt text-white glyphicon"></span>
      //     <span class="icon icon-folder-alt text-white node-icon fa fa-folder fa-lg mr-2"></span>
      //     ${item.name}
      //   </li>`;
      // });

      // var tree = folders.map(item => {
      //   return { text: item.name };
      // });

      // $("#tree").treeview({
      //   data: tree,
      //   backColor: "#f9fbfd",
      //   showBorder: false,
      //   showIcon: true,
      //   nodeIcon: "fa fa-folder fa-lg mr-2"
      // });

      // $("#tree").html(`<ul class="list-group">${tree}</ul>`);

      // setTimeout(() => {
      //   $("#tree li").each(function() {
      //     var folder_name = $(this).text();
      //     if (
      //       folder_name ==
      //       decodeURIComponent((folder + "").replace(/\+/g, "%20"))
      //     ) {
      //       $(this).addClass("active");
      //     }
      //   });
      // }, 1000);

      const urlParams = new URLSearchParams(window.location.search);
      const url = urlParams.get("url");

      let _highlights = await fetch(
        "https://theaisquared.com/app/api/highlights/getByURL",
        {
          method: "POST",
          headers: {
            "Content-type": "application/x-www-form-urlencoded; charset=UTF-8",
          },
          body: `url=${url}&folder=${
            folder != null ? folder : folders[0].name
          }&folder_id=${folder_id > 0 ? folder_id : folders[0].id}&from=popup`,
          // body: `url=${url}`
        }
      );
      let { highlights } = (await _highlights.json()) || [];

      // console.log("=========>", highlights);
      $(".site_title").html("");
      $(".total_highlights").html(0);
      $(".highlight_colors_circles").html("");
      $("#highlights_wrap").html("");

      if (highlights.length) {
        $(".site_title").html(highlights[0].page_title);
        $(".total_highlights").html(highlights.length);
        let color_circles = "";
        let list_html = "";
        let btnfocus = "";
        highlights.forEach((element) => {
          if (element.color != undefined) {
            color_circles += `<span class="color-circle" style="background-color: ${element.color.color_code};"></span>`;
          }
          list_html += `
      <div class="col-md-12 p-1 highlight-wrap" style="border-left: 5px solid ${element.color.color_code};" data-id="${element.id}">
        <div class="highlight-name">${element.content}</div>
        <div class="comments-wrap">`;
          btnfocus += `a.btn_like:focus{color: #fff;}a.btn_dislike:focus{color: #fff;}`;
          let comments_html = "";

          element.comments.forEach((comment) => {
            let img_url = `https://ui-avatars.com/api/?name=${encodeURI(
              comment.user.name
            )}`;
            if (comment.user.profile_pic != null)
              img_url = comment.user.profile_pic;
            comments_html += `<div class="col-md-12 comment-wrap shadow-sm mt-2 mb-2" data-id="${comment.id}">            
              <p class="comment_text text-muted">
              ${comment.comment}
              </p>
              <div class="col-md-12 text-right pb-2 pl-0 pr-0">
                <span style="float:left;margin-top: 5px;">
                  <img src="${img_url}" class="rounded-circle mr-1" width="25" height="25" /> ${comment.user.name}
                </span>
                <a href="javascript:void(0);" class="btn btn-primary btn-sm btn_like"
                  data-id="${comment.id}" data-highlight_id="${comment.highlight_id}">
                    <i class="fa fa-thumbs-up"></i>
                    (${comment.like_count})
                </a>
                <a href="javascript:void(0);" class="btn btn-danger btn-sm btn_dislike"
                  data-id="${comment.id}" data-highlight_id="${comment.highlight_id}">
                  <i class="fa fa-thumbs-down"></i>
                  (${comment.dislike_count})
                </a>
              </div>
            </div>`;
          });

          list_html += `${comments_html}</div></div>`;
        });
        $(".highlight_colors_circles").html(color_circles);
        $("#highlights_wrap").html(list_html);
        $("head").append(`<style>${btnfocus}</style>`);
      }
    };

    // init();

    chrome.runtime.onMessage.addListener((msg, sender, response) => {
      if (msg.from == "content" && msg.subject == "loadHighlights") {
        init();
      }
    });

    $(document).ready(function () {
      setInterval(() => {
        loadFolderTree();
      }, 7000);
    });

    $(document).on("click", ".highlight-name", function () {
      var id = $(this).closest(".highlight-wrap").data("id");
      $(".input-wrap").remove();

      $(this).closest(".highlight-wrap").append(comment_input_wrap);
    });

    $(document).on("click", ".btn-cancel-comment", function () {
      $(".input-wrap").remove();
    });

    $(document).on("click", ".btn-submit-comment", async function () {
      var btn = $(this);
      var btn_text = btn.html();
      var comment = $("#comment").val();
      var highlight_id = $(this).closest(".highlight-wrap").data("id");
      var user_id = result.session_id;

      if ($("#comment").attr("name") == "edit_comment") {
        btn.html(`<i class="fa fa-circle-o-notch fa-spin"></i>`);
        var comment_id = $(this).closest(".comment-wrap").data("id");
        let edit_comment = await fetch(
          `https://theaisquared.com/app/api/highlight-comments/${comment_id}/update`,
          {
            method: "post",
            headers: {
              "Content-type":
                "application/x-www-form-urlencoded; charset=UTF-8",
            },
            body: `comment=${comment}`,
          }
        );
        // btn.html(btn_text);
      } else {
        btn.html(`<i class="fa fa-circle-o-notch"></i>`);
        let new_comment = await fetch(
          "https://theaisquared.com/app/api/highlight-comments",
          {
            method: "POST",
            headers: {
              "Content-type":
                "application/x-www-form-urlencoded; charset=UTF-8",
            },
            body: `highlight_id=${highlight_id}&comment=${comment}&user_id=${user_id}`,
          }
        );
        // btn.html(btn_text);
      }

      init();
    });

    $(document).on("click", ".btn_like", async function () {
      var comment_id = $(this).data("id");
      var highlight_comment_id = $(this).data("highlight_id");
      var user_id = result.session_id;
      var status = 1;

      let new_like = await fetch(
        "https://theaisquared.com/app/api/highlight-comments/like",
        {
          method: "POST",
          headers: {
            "Content-type": "application/x-www-form-urlencoded; charset=UTF-8",
          },
          body: `comment_id=${comment_id}&highlight_comment_id=${highlight_comment_id}&user_id=${user_id}&status=${status}`,
        }
      );

      init();
    });

    $(document).on("click", ".btn_dislike", async function () {
      var comment_id = $(this).data("id");
      var highlight_comment_id = $(this).data("highlight_id");
      var user_id = result.session_id;
      var status = 0;

      let new_dislike = await fetch(
        "https://theaisquared.com/app/api/highlight-comments/dislike",
        {
          method: "POST",
          headers: {
            "Content-type": "application/x-www-form-urlencoded; charset=UTF-8",
          },
          body: `comment_id=${comment_id}&highlight_comment_id=${highlight_comment_id}&user_id=${user_id}&status=${status}`,
        }
      );

      init();
    });

    $(document).on("click", ".ext-draw-folders", function () {
      var leftPane = $(".siderbar-folder");
      if (leftPane.is(":visible")) {
        leftPane.hide();
      } else {
        leftPane.show();
      }
    });

    $(document).on("click", "li.node-tree", function () {
      chrome.storage.sync.set({
        folder: $(this).text(),
        folder_id: $(this).data("folder_id"),
      });
      folder = $(this).text();
      folder_id = $(this).data("folder_id");
      init();
    });

    $(document).on("click", ".comment_text", function () {
      var comment = $(this)
        .closest(".comment-wrap")
        .find(".comment_text")
        .html();
      $(".input-wrap").remove();
      $(this).closest(".comment-wrap").append(comment_input_wrap);
      $(this)
        .closest(".comment-wrap")
        .find("#comment")
        .attr("name", "edit_comment")
        .val(comment.trim());
    });
  }
});
